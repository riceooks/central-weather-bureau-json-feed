﻿<?php
header('Content-Type: application/json; charset=utf-8');

/**
  * Json format
  * @param String $json Json string
  * @return String
  * @author Barry
  */
function json_format($json) {
    $tab = "  ";
    $new_json = "";
    $indent_level = 0;
    $in_string = false;
    $json_obj = json_decode($json);
    if (!$json_obj) {
        return false;
    }
    $json = json_encode($json_obj);
    $len = strlen($json);
    for ($c = 0; $c < $len; $c++) {
        $char = $json[$c];
        switch($char) {
            case '{' :
            case '[' :
                if (!$in_string) {
                    $new_json .= $char . "\n" . str_repeat($tab, $indent_level + 1);
                    $indent_level++;
                } else {
                    $new_json .= $char;
                }
                break;
            case '}' :
            case ']' :
                if (!$in_string) {
                    $indent_level--;
                    $new_json .= "\n" . str_repeat($tab, $indent_level) . $char;
                } else {
                    $new_json .= $char;
                }
                break;
            case ',' :
                if (!$in_string) {
                    $new_json .= ",\n" . str_repeat($tab, $indent_level);
                } else {
                    $new_json .= $char;
                }
                break;
            case ':' :
                if (!$in_string) {
                    $new_json .= ": ";
                } else {
                    $new_json .= $char;
                }
                break;
            case '"' :
                $in_string = !$in_string;
            default :
                $new_json .= $char;
                break;
        }
    }
    return $new_json;
}

/**
  * Get weather data
  * @see http://code.google.com/p/central-weather-bureau/
  */
function getWeather($city){

	$toURL = "http://www.cwb.gov.tw/V7/forecast/taiwan/inc/city/$city.htm";
	$post = array(
	);
	$ch = curl_init();
	$options = array(
		CURLOPT_REFERER=>'',
		CURLOPT_URL=>$toURL,
		CURLOPT_VERBOSE=>0,
		CURLOPT_RETURNTRANSFER=>true,
		CURLOPT_USERAGENT=>"Mozilla/4.0 (compatible;)",
		CURLOPT_POST=>true,
		CURLOPT_POSTFIELDS=>http_build_query($post),
	);
	curl_setopt_array($ch, $options);

	//連接中央氣象局
	$result = curl_exec($ch); 
	curl_close($ch);
	
	preg_match_all('/<table class="FcstBoxTable01" [^>]*[^>]*>(.*)<\/div>/si',$result, $matches, PREG_SET_ORDER);

	preg_match_all('/<td nowrap="nowrap" [^>]*[^>]*>(.*)<\/td>/si',$matches[0][1], $m1, PREG_SET_ORDER);

	$m2 = explode('</td>',$m1[0][1]);
	
	$weather = array();
	for($i=0;$i<=6;$i++){

		preg_match_all('/src=[^>]*[^>](.*)/si',$m2[$i], $m5, PREG_SET_ORDER);//取得天氣圖檔
		$m6 = explode('"',$m5[0][0]);
		$wi='http://www.cwb.gov.tw/V7/'.trim($m6[1],'\.\./\.\./');
		$wtitle = $m6[3];
		$weather[$i]['date'] = date("m-d", mktime(0, 0, 0, date("m"), date("d")+$i+1,date("Y")));
		$weather[$i]['temperature'] = trim(strip_tags($m2[$i]));
		$weather[$i]['title'] = $wtitle;
		$weather[$i]['img'] = $wi;
	}
	
	return ($weather);
	
}

/**
  * Get city list
  * @return Array
  * @author Barry
  */
function getCityList(){
	$arr = array(
		array('key' => 'Taipei_City',
			  'title' => '臺北市'),
		array('key' => 'New_Taipei_City',
			  'title' => '新北市'),
		array('key' => 'Tainan_City',
			  'title' => '臺中市'),
		array('key' => 'Tainan_City',
			  'title' => '臺南市'),
		array('key' => 'Kaohsiung_City',
			  'title' => '高雄市'),
		array('key' => 'Keelung_City',
			  'title' => '基隆市'),
		array('key' => 'Taoyuan_County',
			  'title' => '桃園縣'),
		array('key' => 'Hsinchu_City',
			  'title' => '新竹市'),
		array('key' => 'Hsinchu_County',
			  'title' => '新竹縣'),
		array('key' => 'Miaoli_County',
			  'title' => '苗栗縣'),
		array('key' => 'Changhua_County',
			  'title' => '彰化縣'),
		array('key' => 'Nantou_County',
			  'title' => '南投縣'),
		array('key' => 'Yunlin_County',
			  'title' => '雲林縣'),
		array('key' => 'Chiayi_City',
			  'title' => '嘉義市'),
		array('key' => 'Chiayi_County',
			  'title' => '嘉義縣'),
		array('key' => 'Pingtung_County',
			  'title' => '屏東縣'),
		array('key' => 'Yilan_County',
			  'title' => '宜蘭縣'),
		array('key' => 'Hualien_County',
			  'title' => '花蓮縣'),
		array('key' => 'Taitung_County',
			  'title' => '臺東縣'),
		array('key' => 'Penghu_County',
			  'title' => '澎湖縣'),
		array('key' => 'Kinmen_County',
			  'title' => '金門縣'),
		array('key' => 'Lienchiang_County',
			  'title' => '連江縣')
	);
	
	return $arr;
}

$action = !empty($_REQUEST['action']) ? trim($_REQUEST['action']) : 'default';
$city = !empty($_REQUEST['city']) ? trim($_REQUEST['city']) : 'Taipei_City';

switch( $action ){
	case 'citylist':
		echo json_format( json_encode( array('result' => true, 'data' => getCityList() ) ) );
		break;
	case 'default':
		echo json_format( json_encode( array('result' => true, 'data' => getWeather($city) )) );
		break;
		break;
}
?>
